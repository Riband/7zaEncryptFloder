# 7zaEncryptFloder  

## International English
[Download](https://github.com/Riband/7zaEncryptFloder/releases/download/v0.1.3/7zEncryptFloderV013.7z) 

[Releases](https://github.com/Riband/7zaEncryptFloder/releases)  

[Website](https://riband.github.io/7zaEncryptFloder/)  

Windows only now.  

##### Basic Usage:  
1. Download [sdelete.exe](https://live.sysinternals.com/sdelete.exe) to 7zEncrypt(a floder)
2. Start it:7zEncryptFloder.cmd(Default password 1234)  
3. Change your password:ChangePassWord.cmd  

##### How it work:   

    1.Decrupt the encrypted flie  
    2.You enjoy your private floder
    3.Erease and re-encrypt

**For learning and communication only, please use after volunteering all responsibility**

## Updates
#### V-0.1.4  (2018.7.14)  
* Provide chinese language  
* Fixed some small mistake  

#### V-0.1.3  (2018.6.16)  
* Delete two surplus files  
* Fixed two bugs  

#### V-0.1.2  (2018.6.7)
* Simplified structure, released in self-extracting format and 7z format  
* Update 7-zip Extra Version: 18.05 (2018-04-30)  
* Details optimization  

#### V-0.1.1   
* Fixed:Changepassword.cmd do not erease the inner floder
Translated with the help of Google Translate


## 中文:  
[下载](https://github.com/Riband/7zaEncryptFloder/releases/download/v0.1.3/7zEncryptFloderV013.7z)  

[发布日志](https://github.com/Riband/7zaEncryptFloder/releases)  

[主页](https://riband.github.io/7zaEncryptFloder/)  

暂只支持Windows平台.  

##### 基本用法:  

1. 下载 [sdelete.exe](https://live.sysinternals.com/sdelete.exe) 到7zEncrypt文件夹中  
2. 启动:7zEncryptFloder.cmd(默认密码 1234)  
3. 修改密码:ChangePassWord.cmd  

##### 基本原理:   

    1.加密压缩文件解压  
    2.用户修改解压后的文件夹  
    3.重新加密压缩

**仅供学习交流,请使用者在自愿承担一切责任后使用**

## 更新日志  
#### V-0.1.4  (2018.7.14)  
* 提供汉语  
* 修复几个小错误  

#### V-0.1.3  (2018.6.16)  
* 删除多余文件  
* 修复两个Bug   

#### V-0.1.2  (2018.6.7)  
* 简化结构,改用自解压格式和7z格式发布  
* 更新7-zip Extra Version: 18.05(2018-04-30)  
* 细节优化

#### V-0.1.1   
* 修正:Changepassword.cmd中没有抹去inner文件夹
