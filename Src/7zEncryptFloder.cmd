@echo off

::Echo Info
echo 7zEncryptFloder
echo V-0.1.3
echo By:CloudySummer Lab
echo 7-zip Extra Version: 18.05(2018-04-30)

::expect 
	::%1 SafeZone
	::%2 Password
::set where to seek for 7za and sdelete
set "FuncPath=%~dp07zEncrypt\"
	::rem it when release
	::echo FuncPath = %FuncPath%

::set "SafeZone"
set "SafeZone=%1"
if "%SafeZone%"=="default" (
	set "SafeZone=%FuncPath%"
)
if "%SafeZone%"=="" (
	set "SafeZone=%FuncPath%"
)
	::rem it when release
	::echo "SafeZone=%SafeZone%"

::check sdelete
if exist %FuncPath%sdelete.exe ( echo sdelete exist.) else ( echo please download sdelete.exe in the 7zEncrypt floder! & pause & exit)
echo Sdelete.exe: Copyright (C) 1999-2018 Mark Russinovich

:start
::Get Password
set "password=%2"
if "%password%"=="" (
echo Input your password:
set /p password=:
)

if "%password%"=="default" (
echo Input your password:
set /p password=:
)
			::echo %password%

::Decrypt
%FuncPath%7za.exe x %FuncPath%main.7zf -p%password% -Y -o%SafeZone% > nul
if %errorlevel%==2 (echo Wrong Password! & pause & goto start)
echo Decrypted.

::Open Inner and wait
explorer %SafeZone%inner\
echo Press Any Key to encrypt again...
pause > nul

::Erease1
::echo Ereasing...
::%FuncPath%sdelete.exe %FuncPath%main.7zf > nul
::if %errorlevel%==0 (echo Ereased.) else (echo Error:Can't delete main.7zf & pause & exit)

::ReEncrypt
echo ReEncrypting...
%FuncPath%sdelete.exe %FuncPath%main.7zf -nobanner
%FuncPath%7za a %FuncPath%main.7zf %SafeZone%inner -mhe -mx1 -p%password% > nul

::Erease2
:erase
%FuncPath%sdelete.exe -s %SafeZone%inner -nobanner > nul
::rd %SafeZone%inner
if exist %SafeZone%inner (
	echo Error:Can't delete %SafeZone%inner,you NEED delete it! & pause & goto :erase 
	) else (
	echo %SafeZone%inner Ereased
	)

::End
echo Safe.
pause
