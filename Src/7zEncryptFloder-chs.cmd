@echo off

::Echo Info
echo 7zEncryptFloder
echo V-0.1.3
echo By:云夏工坊
echo 7-zip Extra 版本: 18.05(2018-04-30)

::expect 
	::%1 SafeZone
	::%2 Password
::set where to seek for 7za and sdelete
set "FuncPath=%~dp07zEncrypt\"
	::rem it when release
	::echo FuncPath = %FuncPath%

::set "SafeZone"
set "SafeZone=%1"
if "%SafeZone%"=="default" (
	set "SafeZone=%FuncPath%"
)
if "%SafeZone%"=="" (
	set "SafeZone=%FuncPath%"
)
	::rem it when release
	::echo "SafeZone=%SafeZone%"

::check sdelete
if exist %FuncPath%sdelete.exe ( echo sdelete 存在.) else ( echo 请将 sdelete.exe 下载到 7zEncrypt 文件夹! & pause & exit)
echo Sdelete.exe: Copyright (C) 1999-2018 Mark Russinovich

:start
::Get Password
set "password=%2"
if "%password%"=="" (
echo 输入密码:
set /p password=:
)

if "%password%"=="default" (
echo 输入密码:
set /p password=:
)
			::echo %password%

::Decrypt
%FuncPath%7za.exe x %FuncPath%main.7zf -p%password% -Y -o%SafeZone% > nul
if %errorlevel%==2 (echo 密码错误! & pause & goto start)
echo 已解密.

::Open Inner and wait
explorer %SafeZone%inner\
echo 按任意键重新加密...
pause > nul

::Erease1
::echo Ereasing...
::%FuncPath%sdelete.exe %FuncPath%main.7zf > nul
::if %errorlevel%==0 (echo Ereased.) else (echo Error:Can't delete main.7zf & pause & exit)

::ReEncrypt
echo 重新加密...
%FuncPath%sdelete.exe %FuncPath%main.7zf -nobanner
%FuncPath%7za a %FuncPath%main.7zf %SafeZone%inner -mhe -mx1 -p%password% > nul

::Erease2
:erase
%FuncPath%sdelete.exe -s %SafeZone%inner -nobanner > nul
::rd %SafeZone%inner
if exist %SafeZone%inner (
	echo 错误:无法删除 %SafeZone%inner,务必要删除它! & pause & goto :erase 
	) else (
	echo %SafeZone%inner 已擦除.
	)

::End
echo 安全.
pause
