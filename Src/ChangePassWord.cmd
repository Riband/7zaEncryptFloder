@echo off

::expect 
	::%1 Old Password
	::%2 New Password
	::%3 SafeZone
::debug

::set where to seek for 7za and sdelete
set "FuncPath=%~dp07zEncrypt\"
	::rem it when release
	::echo FuncPath = %FuncPath%

::set "SafeZone"
set "SafeZone=%3"
if "%SafeZone%"=="default" (
	set "SafeZone=%FuncPath%"
)
if "%SafeZone%"=="" (
	set "SafeZone=%FuncPath%"
)
	::rem it when release
	::echo SafeZone = %SafeZone%

::check sdelete
if exist %FuncPath%sdelete.exe ( echo sdelete exist.) else ( echo please download sdelete.exe in the 7zEncrypt floder! & pause & exit)
echo Sdelete.exe: Copyright (C) 1999-2018 Mark Russinovich

:start
::Get Password
set "password=%1"
if "%password%"=="" (
echo Input your old password:
set /p password=:
)

if "%password%"=="default" (
echo Input your old password:
set /p password=:
)

::Get PasswordNew
set "passwordnew=%1"
if "%passwordnew%"=="" (
echo Input your new password:
set /p passwordnew=:
)

if "%passwordnew%"=="default" (
echo Input your new password:
set /p passwordnew=:
)

echo %passwordnew% to %passwordnew%

::Decrypt
%FuncPath%7za.exe x %FuncPath%main.7zf -p%password% -Y -o%SafeZone% > nul
if %errorlevel%==2 (echo Wrong Password! & pause & goto start)
echo Decrypted.

::Erease
echo Ereasing...
%FuncPath%sdelete.exe %FuncPath%main.7zf -nobanner > nul
if %errorlevel%==0 (
	echo Ereased.
	) else (
	echo Error:Can't delete main.7zf & pause & exit
	)

::ReEncrypt
echo ReEncrypting...
%FuncPath%7za a %FuncPath%main.7zf %SafeZone%inner -mhe -p%passwordnew% > nul

if %errorlevel%==0 (echo OK!) else��(echo ERROR!)
%FuncPath%sdelete.exe -s .\inner -nobanner > nul
if exist .\inner (echo Error:Can't delete %SafeZone%inner & pause & exit) else (echo %SafeZone%inner Ereased)

echo Safe.
pause

